﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Musik.Client
{
    internal enum CrudOperation
    {
        Get,
        GetAll,
        Add,
        Remove,
        Edit
    }
}
