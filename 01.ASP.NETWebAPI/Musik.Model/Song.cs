﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Musik.Model
{
    [DataContract(IsReference = true)]
    public class Song
    {
        [Key]
        [DataMember]
        public int SongId { get; set; }

        [Required]
        [DataMember]
        public string Title { get; set; }

        [DataMember]
        public string CreatingYear { get; set; }

        [DataMember]
        public string Ganre { get; set; }

        [ForeignKey("Artist")]
        [DataMember]
        public int ArtistId { get; set; }

        [DataMember]
        public virtual Artist Artist { get; set; }
    }
}
